﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace work1
{
    public partial class Form1 : Form

    {
        public Form1()
        {
            InitializeComponent();
        }
        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("你确定要退出系统吗？", "退出提示",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;

            }
        }
        
        bool isSelected = false;
        Point mouseDownPoint = Point.Empty;
        Point point1, point2, point3, point4;
        Point lineP0 = new Point(0,0);
        Point lineP1=new Point(0,0), lineP2=new Point(0,0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
        List<Point> points = new List<Point>();
        //List<String> letters = new List<String>();
        bool isLine = false;
        int index;
        int menuId;
        float w=90, h=50;
        float l, r;
        Point startPoint,endPoint;
        bool isMenu = false;
        Line line;
        List<BaseGrapthic> list = new List<BaseGrapthic>();
        List<Line> listLines = new List<Line>();
        BinaryFormatter formatter = new BinaryFormatter();

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //删除连线
        private void delete_Click(object sender, EventArgs e)
        {
            Console.WriteLine(list[index].LineList.Count);
            int len = list[index].LineList.Count;
            if ( len > 0)
            {
                while(list[index].LineList.Count>0)
                {
                    Line line1 = list[index].LineList[0];
                    listLines.Remove(line1);
                    if (list[index].isConnected(line1.StartPoint) != 0)
                    {
                        list[index].LineList[0].EndGrapthic.LineList.Remove(line1);
                        list[index].LineList.Remove(line1);
                    }
                    else if(list[index].isConnected(line1.EndPoint) != 0)
                    {
                        list[index].LineList[0].StartGraohic.LineList.Remove(line1);
                        list[index].LineList.Remove(line1);
                    }
                }
            }
            list.Remove(list[index]);
            Refresh();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            for(int i=0;i< list.Count; i++)
            {
                //图元连线
                if (list[i].isConnected(e.X, e.Y) != 0)
                {
                    lineP1 = e.Location;
                    line = new Line();
                    line.Point1 = lineP1;
                    line.StartPoint = lineP1;
                    list[i].Selected = list[i].isConnected(e.X, e.Y);
                    index = i;
                    isLine = true;
                }             
                if (list[i].GetType() == typeof(MyRectangle))
                {
                    MyRectangle myRectangle = (MyRectangle)list[i];
                    Graphics g = this.CreateGraphics();
                    if (myRectangle.contains(e.X,e.Y))
                    {   
                        myRectangle.IsSelected = true;
                        isSelected = true;
                        
                        l = myRectangle.X;
                        r = myRectangle.Y;
                        index = i;
                        mouseDownPoint = e.Location;
                        break;
                        
                    }
                }
                if (list[i].GetType() == typeof(Ellipse))
                {
                    Ellipse ellipse = (Ellipse)list[i];
                    if (ellipse.contains(e.X,e.Y))
                    {
                        ellipse.IsSelected = true;
                        isSelected = true;
                        l = ellipse.X;
                        r = ellipse.Y;
                        index = i;
                        mouseDownPoint = e.Location;
                        break;
                    }
                }
                if (list[i].GetType() == typeof(Circle))
                {
                    Circle myRectangle = (Circle)list[i];
                    if (myRectangle.contains(e.X,e.Y))
                    {
                        myRectangle.IsSelected = true;
                        isSelected = true;
                        l = myRectangle.X;
                        r = myRectangle.Y;
                        index = i;
                        mouseDownPoint = e.Location;
                        break;
                    }
                }
                if (list[i].GetType() == typeof(Parallelogram))
                {
                    Parallelogram parallelogram = (Parallelogram)list[i];
                    if (parallelogram.contains(e.X,e.Y))
                    {
                        parallelogram.IsSelected = true;
                        isSelected = true;
                        point1 = parallelogram.P1;
                        point2 = parallelogram.P2;
                        point3 = parallelogram.P3;
                        point4 = parallelogram.P4;
                        index = i;
                        mouseDownPoint = e.Location;
                        break;
                    }
                }
                if (list[i].GetType() == typeof(Diamond))
                {
                    Diamond diamond = (Diamond)list[i];
                    if (e.X > diamond.Point4.X && e.X < diamond.Point2.X && e.Y > diamond.Point1.Y && e.Y < diamond.Point3.Y)
                    {
                        diamond.IsSelected = true;
                        isSelected = true;
                        point1 = diamond.Point1;
                        point2 = diamond.Point2;
                        point3 = diamond.Point3;
                        point4 = diamond.Point4;
                        index = i;
                        mouseDownPoint = e.Location;
                        break;
                    }
                }
            }
        }
        Font font = new Font(new FontFamily("Times new roman"), 12);

        private void generateCode_Click(object sender, EventArgs e)
        {
            CodeVisitor codeVisitor = new CodeVisitor();
            codeVisitor.Begin();
            for(int i = 0; i < list.Count; i++)
            {
                if (list[i].GetType() == typeof(Ellipse) && list[i].Letter.Equals("开始")) 
                {
                    codeVisitor.Visit((Ellipse)list[i],this.CreateGraphics());
                } 
            }
            FileInfo fi = new FileInfo(@"E:\\work\\work1\\target.txt");
            if (fi.Length != 0)
            {
                MessageBox.Show("成功","添加结果",MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("失败", "添加结果", MessageBoxButtons.RetryCancel);
            }
        }
        private void run_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            runVisitor runVisitor = new runVisitor();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].GetType() == typeof(Ellipse) && list[i].Letter.Equals("开始"))
                {
                    runVisitor.Visit((Ellipse)list[i], this.CreateGraphics());

                }
            }
        }

        Brush brush = new SolidBrush(Color.Black);
       
        
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].contains(e.X, e.Y))
                {
                    Graphics g = this.CreateGraphics();
                    Cursor.Current = Cursors.SizeAll;
                    list[i].drawCircle(g);
                }
                if(list[i].isConnected(e.X, e.Y)!=0)
                {
                    Cursor.Current = Cursors.Cross;
                }
            }
            //不区分图形连线
            if (isLine)
            {
                Graphics g = this.CreateGraphics();
                //设置鼠标样式
                if (list[index].Selected == 1)
                {
                    Refresh();
                    line.move1(lineP1, e, g);
                }
                if (list[index].Selected == 2)
                {
                    Refresh();
                    line.move2(lineP1, e, g);
                }
                if (list[index].Selected == 3)
                {
                    Refresh();
                    line.move3(lineP1, e, g);
                }
                if (list[index].Selected == 4)
                {
                    Refresh();
                    line.move4(lineP1, e, g);
                }

                //判断遇到其他图形
                for (int j = 0; j < list.Count; j++)
                {
                    if (list[j].isConnected(e.X, e.Y) != 0&&j!=index)
                    {
                        if(list[j].isConnected(e.X, e.Y) == 1)
                        {
                            Refresh();
                            if (list[index].Selected == 1)
                            {
                                line.draw11(list[index], list[j]);
                            }
                            if(list[index].Selected == 2)
                            {
                                line.draw21(list[index], list[j]);
                            }   
                            if (list[index].Selected == 4)
                            {
                                line.draw41(list[index], list[j]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw31(list[index], list[j]);
                            }
                            line.draw(g);
                        }
                        if (list[j].isConnected(e.X, e.Y) == 2)
                        {
                            Refresh();
                            if (list[index].Selected == 1)
                            {
                                line.draw12(list[index], list[j]);
                            }
                                
                            if (list[index].Selected == 2)
                            {
                                line.draw22(list[index], list[j]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw32(list[index], list[j]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw42(list[index], list[j]);
                            }
                            line.draw(g);
                        }
                        if (list[j].isConnected(e.X, e.Y) == 3)
                        {
                            Refresh();
                            if (list[index].Selected == 1)
                            {
                                line.draw13(list[index], list[j]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw23(list[index], list[j]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw33(list[index], list[j]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw43(list[index], list[j]);
                            }
                            line.draw(g);
                        }
                        if (list[j].isConnected(e.X, e.Y) == 4)
                        {
                            Refresh();
                            if (list[index].Selected == 1)
                            {
                                line.draw14(list[index], list[j]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw24(list[index], list[j]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw34(list[index], list[j]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw44(list[index], list[j]);
                            }
                            line.draw(g);
                        }
                    }
                }
            }
            //矩形移动
            if (isSelected && list[index].GetType() == typeof(MyRectangle))
            {
                MyRectangle myRectangle = (MyRectangle)list[index];
                Graphics g = this.CreateGraphics();         
                if (isSelected && e.Button == MouseButtons.Left)
                {
                    //线移动
                    if (myRectangle.LineList.Count > 0)
                    {  
                        for (int i = 0; i < myRectangle.LineList.Count; i++)
                        {
                            //移动线出发的矩形
                            if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) != 0)
                            {
                                
                                if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 1)
                                {
                                    if(myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint11(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    }
                                    if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 2)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 90), (int)(r + e.Location.Y - mouseDownPoint.Y+25));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint21(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    }
                                    if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 4)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X), (int)(r + e.Location.Y - mouseDownPoint.Y + 25));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint41(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g); ;
                                    }
                                }
                                if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 2)
                                {
                                    if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint12(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    }
                                        
                                }
                                if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 3)
                                {
                                    if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint13(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    }
                                        
                                }
                                if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 4)
                                {
                                    if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        myRectangle.LineList[i].Point1 = p;
                                        myRectangle.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        myRectangle.LineList[i].movePoint14(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    }
                                       
                                }
                            }
                            //移动线指向的矩形
                            if (myRectangle.isConnected(myRectangle.LineList[i].EndPoint) != 0)
                            {
                                Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                if (!myRectangle.LineList[i].Point6.IsEmpty)
                                    myRectangle.LineList[i].Point6 = p;
                                else if(!myRectangle.LineList[i].Point5.IsEmpty)
                                    myRectangle.LineList[i].Point5 = p;
                                else if (!myRectangle.LineList[i].Point4.IsEmpty)
                                    myRectangle.LineList[i].Point4 = p;
                                else if (!myRectangle.LineList[i].Point4.IsEmpty)
                                    myRectangle.LineList[i].Point3= p;
                                else if (!myRectangle.LineList[i].Point2.IsEmpty)
                                    myRectangle.LineList[i].Point2 = p;
                                myRectangle.LineList[i].EndPoint = p;
                                this.Refresh();
                                if (myRectangle.isConnected(myRectangle.LineList[i].EndPoint) == 1)
                                {
                                    //Console.WriteLine("111");
                                    if (myRectangle.LineList[i].StartGraohic.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                    {
                                        myRectangle.LineList[i].movePoint1_1(myRectangle.LineList[i].Point1, myRectangle.LineList[i].EndPoint, g);
                                    }
                                    
                                }
                            }          
                        }   
                    }
                    myRectangle.X = l + e.Location.X - mouseDownPoint.X;
                    myRectangle.Y = r + e.Location.Y - mouseDownPoint.Y;
                    Point p1 = new Point((int)myRectangle.X + 45, (int)myRectangle.Y);
                    Point p2 = new Point((int)myRectangle.X + 90, (int)myRectangle.Y + 25);
                    Point p3 = new Point((int)myRectangle.X + 45, (int)myRectangle.Y + 50);
                    Point p4 = new Point((int)myRectangle.X, (int)myRectangle.Y + 25);
                    myRectangle.Point1 = p1;
                    myRectangle.Point2 = p2;
                    myRectangle.Point3 = p3;
                    myRectangle.Point4 = p4;
                    Refresh();
                    //myRectangle.draw(g);    
                }
            }
            if (isSelected && list[index].GetType() == typeof(Ellipse))
            {
                Ellipse ellipse = (Ellipse)list[index];
                Graphics g = this.CreateGraphics();
                if (isSelected && e.Button == MouseButtons.Left)
                {
                    if (ellipse.LineList.Count > 0)
                    {
                        for (int i = 0; i < ellipse.LineList.Count; i++)
                        {
                            //移动线出发的圆形
                            if (ellipse.isConnected(ellipse.LineList[i].StartPoint) != 0)
                            {      
                                if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 1)
                                {
                                    //移动11
                                    if(ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint11(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }
                                    //移动21
                                    if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 2)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 90), (int)(r + e.Location.Y - mouseDownPoint.Y+25));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint21(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }
                                    if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 4)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X), (int)(r + e.Location.Y - mouseDownPoint.Y + 25));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint41(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }

                                }
                                if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 2)
                                {
                                    if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint12(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }

                                }
                                if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 3)
                                {
                                    if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint13(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }

                                }
                                if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 4)
                                {
                                    if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                        ellipse.LineList[i].Point1 = p;
                                        ellipse.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        ellipse.LineList[i].movePoint14(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    }

                                }
                            }
                            //移动线指向的矩形
                            if (ellipse.isConnected(ellipse.LineList[i].EndPoint) != 0)
                            {
                                Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                if (!ellipse.LineList[i].Point6.IsEmpty)
                                    ellipse.LineList[i].Point6 = p;
                                else if (!ellipse.LineList[i].Point5.IsEmpty)
                                    ellipse.LineList[i].Point5 = p;
                                else if (!ellipse.LineList[i].Point4.IsEmpty)
                                    ellipse.LineList[i].Point4 = p;
                                else if (!ellipse.LineList[i].Point4.IsEmpty)
                                    ellipse.LineList[i].Point3 = p;
                                else if (!ellipse.LineList[i].Point2.IsEmpty)
                                    ellipse.LineList[i].Point2 = p;
                                ellipse.LineList[i].EndPoint = p;
                                this.Refresh();
                                if (ellipse.isConnected(ellipse.LineList[i].EndPoint) == 1)
                                {
                                    //Console.WriteLine("111");
                                    if (ellipse.LineList[i].StartGraohic.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                    {
                                        ellipse.LineList[i].movePoint1_1(ellipse.LineList[i].Point1, ellipse.LineList[i].EndPoint, g);
                                    }

                                }
                            }
                        }
                    }
                    ellipse.X = l + e.Location.X - mouseDownPoint.X;
                    ellipse.Y = r + e.Location.Y - mouseDownPoint.Y;
                    Point p1 = new Point((int)ellipse.X + 45, (int)ellipse.Y);
                    Point p2 = new Point((int)ellipse.X + 90, (int)ellipse.Y + 25);
                    Point p3 = new Point((int)ellipse.X + 45, (int)ellipse.Y + 50);
                    Point p4 = new Point((int)ellipse.X, (int)ellipse.Y + 25);
                    ellipse.Point1 = p1;
                    ellipse.Point2 = p2;
                    ellipse.Point3 = p3;
                    ellipse.Point4 = p4;
                    this.Refresh();
                }
            }
            if (isSelected && list[index].GetType() == typeof(Parallelogram))
            {
                Parallelogram parallelogram = (Parallelogram)list[index];
                Graphics g = this.CreateGraphics();
                if (isSelected && e.Button == MouseButtons.Left)
                {
                    if (parallelogram.LineList.Count > 0)
                    {
                        for (int i = 0; i < parallelogram.LineList.Count; i++)
                        {
                            //移动线出发的平行四边形
                            if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) != 0)
                            {
                               
                                if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 1)
                                {
                                    //移动11
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint11(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }
                                    //移动21
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 2)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 80), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y)+30);
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint21(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 4)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X-10), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y) + 30);
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint41(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }
                                }
                                if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 2)
                                {
                                   
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint12(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }

                                }
                                if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 3)
                                {
     
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint13(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }

                                }
                                if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 4)
                                {
                                    if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                    {
                                        Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                        parallelogram.LineList[i].Point1 = p;
                                        parallelogram.LineList[i].StartPoint = p;
                                        this.Refresh();
                                        parallelogram.LineList[i].movePoint14(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    }

                                }
                            }
                            //移动线指向的平行四边形
                            if (parallelogram.isConnected(parallelogram.LineList[i].EndPoint) != 0)
                            {
                                Point p = new Point((int)(parallelogram.Point1.X + e.Location.X - mouseDownPoint.X), (int)(parallelogram.Point1.Y + e.Location.Y - mouseDownPoint.Y));
                                if (!parallelogram.LineList[i].Point6.IsEmpty)
                                    parallelogram.LineList[i].Point6 = p;
                                else if (!parallelogram.LineList[i].Point5.IsEmpty)
                                    parallelogram.LineList[i].Point5 = p;
                                else if (!parallelogram.LineList[i].Point4.IsEmpty)
                                    parallelogram.LineList[i].Point4 = p;
                                else if (!parallelogram.LineList[i].Point4.IsEmpty)
                                    parallelogram.LineList[i].Point3 = p;
                                else if (!parallelogram.LineList[i].Point2.IsEmpty)
                                    parallelogram.LineList[i].Point2 = p;
                                parallelogram.LineList[i].EndPoint = p;
                                this.Refresh();
                                if (parallelogram.isConnected(parallelogram.LineList[i].EndPoint) == 1)
                                {
                                    //Console.WriteLine("111");
                                    if (parallelogram.LineList[i].StartGraohic.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                    {
                                        parallelogram.LineList[i].movePoint1_1(parallelogram.LineList[i].Point1, parallelogram.LineList[i].EndPoint, g);
                                    }

                                }
                            }
                        }
                    }
                    Point p1 = new Point(point1.X + e.Location.X - mouseDownPoint.X, point1.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p2 = new Point(point2.X + e.Location.X - mouseDownPoint.X, point2.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p3 = new Point(point3.X + e.Location.X - mouseDownPoint.X, point3.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p4 = new Point(point4.X + e.Location.X - mouseDownPoint.X, point4.Y + e.Location.Y - mouseDownPoint.Y);
                    parallelogram.P1 = p1;
                    parallelogram.P2 = p2;
                    parallelogram.P3 = p3;
                    parallelogram.P4 = p4;
                    parallelogram.Point1 = p1;
                    parallelogram.Point2 = p2;
                    parallelogram.Point3 = p3;
                    parallelogram.Point4 = p4;
                    this.Refresh();
                    //list[index].draw(g);
                }
            }
            if (isSelected && list[index].GetType() == typeof(Diamond))
            {
                Diamond diamond = (Diamond)list[index];
                Graphics g = this.CreateGraphics();
                if (diamond.LineList.Count > 0)
                {
                    for (int i = 0; i < diamond.LineList.Count; i++)
                    {
                        //移动线出发的菱形
                        if (diamond.isConnected(diamond.LineList[i].StartPoint) != 0)
                        {
                          
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 1)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint11(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 2)
                                {
                                    Point p = new Point((int)(point2.X + e.Location.X - mouseDownPoint.X), (int)(point2.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint21(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 4)
                                {
                                    Point p = new Point((int)(point4.X + e.Location.X - mouseDownPoint.X), (int)(point4.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint41(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }
                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 2)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint12(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }

                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 3)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint13(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }

                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 4)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].StartPoint = p;
                                    this.Refresh();
                                    diamond.LineList[i].movePoint14(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                }

                            }
                        }
                        //移动线指向的平行四边形
                        if (diamond.isConnected(diamond.LineList[i].EndPoint) != 0)
                        {
                            Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                            if (!diamond.LineList[i].Point6.IsEmpty)
                                diamond.LineList[i].Point6 = p;
                            else if (!diamond.LineList[i].Point5.IsEmpty)
                                diamond.LineList[i].Point5 = p;
                            else if (!diamond.LineList[i].Point4.IsEmpty)
                                diamond.LineList[i].Point4 = p;
                            else if (!diamond.LineList[i].Point4.IsEmpty)
                                diamond.LineList[i].Point3 = p;
                            else if (!diamond.LineList[i].Point2.IsEmpty)
                                diamond.LineList[i].Point2 = p;
                            diamond.LineList[i].EndPoint = p;
                            this.Refresh();
                            if (diamond.isConnected(diamond.LineList[i].EndPoint) == 1)
                            {
                                if (diamond.LineList[i].StartGraohic.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    diamond.LineList[i].movePoint1_1(diamond.LineList[i].Point1, diamond.LineList[i].EndPoint, g);
                                }

                            }
                        }
                    }
                }
                if (isSelected && e.Button == MouseButtons.Left)
                {
                    Point p1 = new Point(point1.X + e.Location.X - mouseDownPoint.X, point1.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p2 = new Point(point2.X + e.Location.X - mouseDownPoint.X, point2.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p3 = new Point(point3.X + e.Location.X - mouseDownPoint.X, point3.Y + e.Location.Y - mouseDownPoint.Y);
                    Point p4 = new Point(point4.X + e.Location.X - mouseDownPoint.X, point4.Y + e.Location.Y - mouseDownPoint.Y);
                    diamond.Point1 = p1;
                    diamond.Point2 = p2;
                    diamond.Point3 = p3;
                    diamond.Point4 = p4;
                    this.Refresh();
                    //list[index].draw(g);
                }
            }
            if (isSelected && list[index].GetType() == typeof(Circle))
            {
                Graphics g = this.CreateGraphics();
                Circle myRectangle=(Circle)list[index];
                if (isSelected && e.Button == MouseButtons.Left)
                {
                    Point p1 = new Point((int)myRectangle.X + 10, (int)myRectangle.Y);
                    Point p2 = new Point((int)myRectangle.X + 20, (int)myRectangle.Y + 10);
                    Point p3 = new Point((int)myRectangle.X + 10, (int)myRectangle.Y + 20);
                    Point p4 = new Point((int)myRectangle.X, (int)myRectangle.Y + 10);
                    myRectangle.Point1 = p1;
                    myRectangle.Point2 = p2;
                    myRectangle.Point3 = p3;
                    myRectangle.Point4 = p4;
                    myRectangle.X = l + e.Location.X - mouseDownPoint.X;
                    myRectangle.Y = r + e.Location.Y - mouseDownPoint.Y;
                    this.Refresh();
                }
            }

        }
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            
            //画线
            if (isLine)
            {
                isLine = false;
               
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].isConnected(e.X, e.Y) != 0 && i != index)
                    {
                        Console.WriteLine(list[i].isConnected(e.X, e.Y));
                        
                        if (list[i].isConnected(e.X, e.Y) == 1)
                        {
                            if(list[index].Selected==1)
                            {
                                line.draw11(list[index], list[i]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw21(list[index], list[i]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw31(list[index], list[i]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw41(list[index], list[i]);
                            }
                            line.EndPoint=line.GetEnd();
                            line.StartGraohic = list[index];
                            line.EndGrapthic = list[i];
                            list[index].LineList.Add(line);
                            list[i].LineList.Add(line);
                            listLines.Add(line);
                        }
                        if (list[i].isConnected(e.X, e.Y) == 2)
                        {
                            if (list[index].Selected == 1)
                            {
                                line.draw12(list[index], list[i]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw22(list[index], list[i]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw32(list[index], list[i]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw42(list[index], list[i]);
                            }
                            line.EndPoint = line.GetEnd();
                            line.StartGraohic = list[index];
                            line.EndGrapthic = list[i];
                            list[index].LineList.Add(line);
                            list[i].LineList.Add(line);
                            listLines.Add(line);
                        }
                        if (list[i].isConnected(e.X, e.Y) == 3)
                        {
                            if (list[index].Selected == 1)
                            {
                                line.draw13(list[index], list[i]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw23(list[index], list[i]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw33(list[index], list[i]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw43(list[index], list[i]);
                            }
                            line.EndPoint = line.GetEnd();
                            line.StartGraohic = list[index];
                            line.EndGrapthic = list[i];
                            list[index].LineList.Add(line);
                            list[i].LineList.Add(line);
                            listLines.Add(line);
                        }
                        if (list[i].isConnected(e.X, e.Y) == 4)
                        {
                            if (list[index].Selected == 1)
                            {
                                line.draw14(list[index], list[i]);
                            }
                            if (list[index].Selected == 2)
                            {
                                line.draw24(list[index], list[i]);
                            }
                            if (list[index].Selected == 3)
                            {
                                line.draw34(list[index], list[i]);
                            }
                            if (list[index].Selected == 4)
                            {
                                line.draw44(list[index], list[i]);
                            }
                            line.EndPoint = line.GetEnd();
                            line.StartGraohic = list[index];
                            line.EndGrapthic = list[i];
                            list[index].LineList.Add(line);
                            list[i].LineList.Add(line);
                            listLines.Add(line);
                        }
                    }
                    
                }              
            }
            //右击显示菜单
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].contains(e.X, e.Y)&&e.Button==MouseButtons.Right)
                { 
                    deleteMenuStrip.Show(this,e.X,e.Y);
                    menuId = i;
                }
              
            }
            //移动
            if (isSelected && list[index].GetType() == typeof(MyRectangle))
            {
                MyRectangle myRectangle = (MyRectangle)list[index];
                Graphics g = this.CreateGraphics();
                if (myRectangle.LineList.Count > 0)
                {
                    for (int i = 0; i < myRectangle.LineList.Count; i++)
                    {    
                        //线的出发结点
                        if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) != 0)
                        {
                            if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 1)
                            {
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint11(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 2)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 90), (int)(r + e.Location.Y - mouseDownPoint.Y+25));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint21(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 4)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X), (int)(r + e.Location.Y - mouseDownPoint.Y + 25));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint41(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }

                            }
                            if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 2)
                            {
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint12(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }
                                
                            }
                            if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 3)
                            {
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint13(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }
                                
                            }
                            if (myRectangle.LineList[i].EndGrapthic.isConnected(myRectangle.LineList[i].EndPoint) == 4)
                            {
                                if (myRectangle.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint14(myRectangle.LineList[i].EndPoint, myRectangle.LineList[i].Point1, g);
                                    myRectangle.LineList[i].StartPoint = myRectangle.LineList[i].Point1;
                                }
                                
                            }
                        }
                        //线的终止结点
                        if (myRectangle.isConnected(myRectangle.LineList[i].EndPoint) != 0)
                        {
                            if (myRectangle.isConnected(myRectangle.LineList[i].EndPoint) == 1)
                            {
                                if (myRectangle.LineList[i].StartGraohic.isConnected(myRectangle.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    myRectangle.LineList[i].Point1 = p;
                                    myRectangle.LineList[i].movePoint1_1(myRectangle.LineList[i].Point1, myRectangle.LineList[i].EndPoint, g);
                                    myRectangle.LineList[i].EndPoint = myRectangle.LineList[i].Point4;
                                }
                                    
                            }  
                        }
                    }
                }
                l = myRectangle.X;
                r = myRectangle.Y;
                Point p1 = new Point((int)myRectangle.X + 45, (int)myRectangle.Y);
                Point p2 = new Point((int)myRectangle.X + 90, (int)myRectangle.Y + 25);
                Point p3 = new Point((int)myRectangle.X + 45, (int)myRectangle.Y + 50);
                Point p4 = new Point((int)myRectangle.X, (int)myRectangle.Y + 25);
                myRectangle.Point1 = p1;
                myRectangle.Point2 = p2;
                myRectangle.Point3 = p3;
                myRectangle.Point4 = p4;
                myRectangle.IsSelected = false;
                isSelected = false;
            }
            if (isSelected && list[index].GetType() == typeof(Ellipse))
            {

                Ellipse ellipse = (Ellipse)list[index];
                Graphics g = this.CreateGraphics();
                if (ellipse.LineList.Count > 0)
                {
                    for (int i = 0; i < ellipse.LineList.Count; i++)
                    {
                        
                        //线的出发结点
                        if (ellipse.isConnected(ellipse.LineList[i].StartPoint) != 0)
                        {
                            if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 1)
                            {
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint11(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 2)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 90), (int)(r + e.Location.Y - mouseDownPoint.Y + 25));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint21(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 4)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X), (int)(r + e.Location.Y - mouseDownPoint.Y + 25));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint41(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }

                            }
                            if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 2)
                            {
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint12(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }

                            }
                            if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 3)
                            {
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint13(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }

                            }
                            if (ellipse.LineList[i].EndGrapthic.isConnected(ellipse.LineList[i].EndPoint) == 4)
                            {
                                if (ellipse.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(l + e.Location.X - mouseDownPoint.X + 45), (int)(r + e.Location.Y - mouseDownPoint.Y));
                                    ellipse.LineList[i].Point1 = p;
                                    ellipse.LineList[i].movePoint14(ellipse.LineList[i].EndPoint, ellipse.LineList[i].Point1, g);
                                    ellipse.LineList[i].StartPoint = ellipse.LineList[i].Point1;
                                }

                            }
                        }
                        //线的终止结点
                        if (ellipse.isConnected(ellipse.LineList[i].EndPoint) != 0)
                        {
                            if (ellipse.isConnected(ellipse.LineList[i].EndPoint) == 1)
                            {
                                if (ellipse.LineList[i].StartGraohic.isConnected(ellipse.LineList[i].StartPoint) == 1)
                                {
                                    ellipse.LineList[i].movePoint1_1(ellipse.LineList[i].Point1, ellipse.LineList[i].EndPoint, g);
                                    ellipse.LineList[i].EndPoint = ellipse.LineList[i].Point4;
                                }

                            }
                        }
                    }
                }
                l = ellipse.X;
                r = ellipse.Y;
                Point p1 = new Point((int)ellipse.X + 45, (int)ellipse.Y);
                Point p2 = new Point((int)ellipse.X + 90, (int)ellipse.Y + 25);
                Point p3 = new Point((int)ellipse.X + 45, (int)ellipse.Y + 50);
                Point p4 = new Point((int)ellipse.X, (int)ellipse.Y + 25);
                ellipse.Point1 = p1;
                ellipse.Point2 = p2;
                ellipse.Point3 = p3;
                ellipse.Point4 = p4;
                ellipse.IsSelected = false;
                isSelected = false;
            }
            if (isSelected && list[index].GetType() == typeof(Parallelogram))
            {
                Parallelogram parallelogram = (Parallelogram)list[index];
                Graphics g = this.CreateGraphics();
                if (parallelogram.LineList.Count > 0)
                {
                    for (int i = 0; i < parallelogram.LineList.Count; i++)
                    {
                        
                        //线的出发结点
                        if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) != 0)
                        {
                            if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 1)
                            {
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint11(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }
                               
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 2)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 80), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y) + 30);
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint21(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 4)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X-10), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y) + 30);
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint41(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }
                            }
                            if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 2)
                            {
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint12(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }

                            }
                            if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 3)
                            {
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint13(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }

                            }
                            if (parallelogram.LineList[i].EndGrapthic.isConnected(parallelogram.LineList[i].EndPoint) == 4)
                            {
                                if (parallelogram.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X + 35), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    parallelogram.LineList[i].Point1 = p;
                                    parallelogram.LineList[i].movePoint14(parallelogram.LineList[i].EndPoint, parallelogram.LineList[i].Point1, g);
                                    parallelogram.LineList[i].StartPoint = parallelogram.LineList[i].Point1;
                                }

                            }
                        }
                        //线的终止结点
                        if (parallelogram.isConnected(parallelogram.LineList[i].EndPoint) != 0)
                        {
                            if (parallelogram.isConnected(parallelogram.LineList[i].EndPoint) == 1)
                            {
                                if (parallelogram.LineList[i].StartGraohic.isConnected(parallelogram.LineList[i].StartPoint) == 1)
                                {
                                    parallelogram.LineList[i].movePoint1_1(parallelogram.LineList[i].Point1, parallelogram.LineList[i].EndPoint, g);
                                    parallelogram.LineList[i].EndPoint = parallelogram.LineList[i].Point4;
                                }

                            }
                        }
                    }
                }
                point1 = parallelogram.P1;
                point2 = parallelogram.P2;
                point3 = parallelogram.P3;
                point4 = parallelogram.P4;
                Point p1 = new Point((int)parallelogram.P1.X + 35, (int)parallelogram.P1.Y);
                Point p2 = new Point((int)parallelogram.P3.X + 10, (int)parallelogram.P2.Y + 30);
                Point p3 = new Point((int)parallelogram.P4.X + 55, (int)parallelogram.P3.Y);
                Point p4 = new Point((int)parallelogram.P4.X + 10, (int)parallelogram.P1.Y + 30);
                parallelogram.Point1 = p1;
                parallelogram.Point2 = p2;
                parallelogram.Point3 = p3;
                parallelogram.Point4 = p4;
                parallelogram.IsSelected = false;
                isSelected = false;
            }
            if (isSelected && list[index].GetType() == typeof(Diamond))
            {
                Diamond diamond = (Diamond)list[index];
                Graphics g = this.CreateGraphics();
                if (diamond.LineList.Count > 0)
                {
                    for (int i = 0; i < diamond.LineList.Count; i++)
                    {
                        
                        //线的出发结点
                        if (diamond.isConnected(diamond.LineList[i].StartPoint) != 0)
                        {
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 1)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint11(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 2)
                                {
                                    Point p = new Point((int)(point2.X + e.Location.X - mouseDownPoint.X), (int)(point2.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint21(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 4)
                                {
                                    Point p = new Point((int)(point4.X + e.Location.X - mouseDownPoint.X), (int)(point4.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint41(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }
                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 2)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint12(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }

                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 3)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint13(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }

                            }
                            if (diamond.LineList[i].EndGrapthic.isConnected(diamond.LineList[i].EndPoint) == 4)
                            {
                                if (diamond.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    Point p = new Point((int)(point1.X + e.Location.X - mouseDownPoint.X), (int)(point1.Y + e.Location.Y - mouseDownPoint.Y));
                                    diamond.LineList[i].Point1 = p;
                                    diamond.LineList[i].movePoint14(diamond.LineList[i].EndPoint, diamond.LineList[i].Point1, g);
                                    diamond.LineList[i].StartPoint = diamond.LineList[i].Point1;
                                }

                            }
                        }
                        //线的终止结点
                        if (diamond.isConnected(diamond.LineList[i].EndPoint) != 0)
                        {
                            if (diamond.isConnected(diamond.LineList[i].EndPoint) == 1)
                            {
                                if (diamond.LineList[i].StartGraohic.isConnected(diamond.LineList[i].StartPoint) == 1)
                                {
                                    diamond.LineList[i].movePoint1_1(diamond.LineList[i].Point1, diamond.LineList[i].EndPoint, g);
                                    diamond.LineList[i].EndPoint = diamond.LineList[i].Point4;
                                }

                            }
                        }
                    }
                }
                point1 = diamond.Point1;
                point2 = diamond.Point2;
                point3 = diamond.Point3;
                point4 = diamond.Point4;
                diamond.IsSelected = false;
                isSelected = false;
            }
            if (isSelected && list[index].GetType() == typeof(Circle))
            {
                Circle myRectangle = (Circle)list[index];
                l = myRectangle.X;
                r = myRectangle.Y;
                Point p1 = new Point((int)myRectangle.X + 10, (int)myRectangle.Y);
                Point p2 = new Point((int)myRectangle.X + 20, (int)myRectangle.Y + 10);
                Point p3 = new Point((int)myRectangle.X + 10, (int)myRectangle.Y + 20);
                Point p4 = new Point((int)myRectangle.X, (int)myRectangle.Y + 10);
                myRectangle.Point1 = p1;
                myRectangle.Point2 = p2;
                myRectangle.Point3 = p3;
                myRectangle.Point4 = p4;
                myRectangle.IsSelected = false;
                isSelected = false;
            }
        }
        private void connect_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            BaseGrapthic myRectangle = new Circle();
            list.Add(myRectangle);
            myRectangle.draw(g);
        }

        private void Form1_MouseHover(object sender, EventArgs e)
        {
            //this.Cursor = Cursor.Current;
        }

        //弹出输入框
        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].contains(e.X, e.Y))
                {
                    string str = Interaction.InputBox("请输入代码", "输入框", "", e.X,e.Y);
                    Graphics g = this.CreateGraphics();
                    list[i].Letter = str;
                    list[i].drawString(g);
                    Refresh();
                }
            }
            for(int i = 0; i < listLines.Count; i++)
            {
                if (listLines[i].contains(e.X, e.Y))
                {
                    string str = Interaction.InputBox("请输入Y/N", "输入框", "", e.X, e.Y);
                    Graphics g = this.CreateGraphics();
                    RectangleF myRectangle = new RectangleF(e.X - 10, e.Y-10, 20, 20);
                    listLines[i].Letter = str;
                    listLines[i].RectangleF = myRectangle;
                    listLines[i].drawString(g);
                    Refresh();
                }
            }
            
        }

        private void in_out_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            BaseGrapthic parallelogram = new Parallelogram();
            Serializer.ObjectToFile(parallelogram,@"E:\work\work1\BaseGraphic.txt");
            list.Add(parallelogram);
            parallelogram.draw(g);
        }

        private void address_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            BaseGrapthic rectangle = new MyRectangle();
            Serializer.ObjectToFile(rectangle, @"E:\work\work1\BaseGraphic.txt");
            list.Add(rectangle);
            rectangle.draw(g);
        }

        private void judge_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            BaseGrapthic diamand = new Diamond();
            Serializer.ObjectToFile(diamand, @"E:\work\work1\BaseGraphic.txt");
            list.Add(diamand);
            diamand.draw(g);
        }

        private void start_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            BaseGrapthic ellipse = new Ellipse();
            Serializer.ObjectToFile(ellipse, @"E:\work\work1\BaseGraphic.txt");
            list.Add(ellipse);
            ellipse.draw(g);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].draw(this.CreateGraphics());
                if(list[i].Letter!=null)
                    list[i].drawString(this.CreateGraphics());
            }
            for (int i = 0; i < listLines.Count; i++)
            {
                listLines[i].draw(this.CreateGraphics());
                if (listLines[i].Letter != null)
                    listLines[i].drawString(this.CreateGraphics());
            }

        }

        

        
       
    }
}
