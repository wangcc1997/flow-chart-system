﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public class Diamond : BaseGrapthic
    {
        private Point point1;
        private Point point2;
        private Point point3;
        private Point point4;
        private bool isSelected;
        private List<Line> lineList;
        public Diamond()
        {
            point1.X = 100;
            point1.Y = 40;
            point2.X = 145;
            point2.Y = 65;
            point3.X = 100;
            point3.Y = 90;
            point4.X = 55;
            point4.Y = 65;
            isSelected = false;
            this.LineList = new List<Line>();
        }

        public override Point Point1 { get => point1; set => point1 = value; }
        public override Point Point2 { get => point2; set => point2 = value; }
        public override Point Point3 { get => point3; set => point3 = value; }
        public override Point Point4 { get => point4; set => point4 = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public override List<Line> LineList { get => lineList; set => lineList = value; }

       

        public override void add()
        {
            throw new NotImplementedException();
        }

        public override bool contains(int x, int y)
        {
            float rx = this.point4.X + 40;
            float ry = this.point1.Y + 40;
            float d1 = (float)Math.Pow(rx - x, 2);
            float d2 = (float)Math.Pow(ry - y, 2);
            float d = (float)Math.Sqrt(d1 + d2);
            if (d < 25)
            {
                return true;
            }
            else return false;
        }

        public override bool contains(Point point)
        {
            float rx = this.point4.X + 40;
            float ry = this.point1.Y + 40;
            float d1 = (float)Math.Pow(rx - point.X, 2);
            float d2 = (float)Math.Pow(ry - point.Y, 2);
            float d = (float)Math.Sqrt(d1 + d2);
            if (d < 25)
            {
                return true;
            }
            else return false;
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Point[] points = { point1, point2, point3, point4 };
            g.DrawPolygon(p, points);
            p.Dispose();
            g.Dispose();
        }


        public override void drawCircle(Graphics g)
        {
            float x1 = point1.X  - 4;
            float y1 = point1.Y - 4;
            float x2 = point2.X - 4;
            float y2 = point2.Y - 4;
            float x3 = point3.X - 4;
            float y3 = point3.Y - 4;
            float x4 = point4.X - 4;
            float y4 = point4.Y - 4;
            Rectangle rect1 = new Rectangle((int)x1, (int)y1, 8, 8);
            Rectangle rect2 = new Rectangle((int)x2, (int)y2, 8, 8);
            Rectangle rect3 = new Rectangle((int)x3, (int)y3, 8, 8);
            Rectangle rect4 = new Rectangle((int)x4, (int)y4, 8, 8);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, rect1);
            g.DrawEllipse(pen, rect2);
            g.DrawEllipse(pen, rect3);
            g.DrawEllipse(pen, rect4);
        }

        public override int isConnected(int x, int y)
        {
            if (x > point1.X - 5 && x < point1.X+5 && y > point1.Y - 5 && y < point1.Y + 5)
            {
                return 1;
            }
            else if (x > point2.X - 5 && x < point2.X + 5  && y > point2.Y - 5 && y < point2.Y + 5)
            {
                return 2;
            }
            else if (x > point3.X - 5 && x < point3.X + 5  && y > point3.Y - 5 && y < point3.Y + 5)
            {
                return 3;
            }
            else if (x > point4.X - 5 && x < point4.X + 5  && y > point4.Y - 5 && y < point4.Y + 5)
            {
                return 4;
            }
            else return 0;
        }

        public override int isConnected(Point point)
        {
            if (point.X > point1.X - 5 && point.X < point1.X + 5 && point.Y > point1.Y - 5 && point.Y < point1.Y + 5)
            {
                return 1;
            }
            else if (point.X > point2.X - 5 && point.X < point2.X + 5 && point.Y > point2.Y - 5 && point.Y < point2.Y + 5)
            {
                return 2;
            }
            else if (point.X > point3.X - 5 && point.X < point3.X + 5 && point.Y > point3.Y - 5 && point.Y < point3.Y + 5)
            {
                return 3;
            }
            else if (point.X > point4.X - 5 && point.X < point4.X + 5 && point.Y > point4.Y - 5 && point.Y < point4.Y + 5)
            {
                return 4;
            }
            else return 0;
        }

        public override void remove()
        {
            throw new NotImplementedException();
        }
        public override void Accept(Visitor visitor,Graphics g)
        {
            visitor.Visit(this,g);
        }

        public override void fill(Graphics g, Color color)
        {
            Point[] points = {this.point1,this.Point2,this.Point3,this.point4 };
            g.FillPolygon(new SolidBrush(color), points);
            this.drawString(g);
        }
    }
}
