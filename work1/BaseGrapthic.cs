﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public abstract class BaseGrapthic
    {
        private int selected;
        private String letter;
        public abstract Point Point1 { get; set; }
        public abstract Point Point2 { get; set; }
        public abstract Point Point3 { get; set; }
        public abstract Point Point4 { get; set; }

        public abstract List<Line> LineList { get; set; }
        public abstract void Accept(Visitor visitor,Graphics g);

        public int Selected { get => selected; set => selected = value; }
        public string Letter { get => letter; set => letter = value; }

        public abstract void draw(Graphics g);
        public void drawString(Graphics g)
        {
            Font font = new Font(new FontFamily("Times new roman"), 12);
            Brush brush = new SolidBrush(Color.Black);
            RectangleF rect = new RectangleF(this.Point4.X + 10, this.Point1.Y, 80, 40);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.DrawString(this.Letter, font, brush, rect, stringFormat);
           
        }

        public abstract void fill(Graphics g,Color color);
        public abstract void add();
        public abstract void remove();
        public abstract bool contains(int x, int y);
        public abstract bool contains(Point point);
        public abstract int isConnected(int x,int y);
        public abstract int isConnected(Point point);
        public abstract void drawCircle(Graphics g);
    }
}
