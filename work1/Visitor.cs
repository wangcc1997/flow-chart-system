﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work1
{
    public interface Visitor
    {
        void Visit(MyRectangle myRectangle, Graphics g);
        void Visit(Diamond diamond, Graphics g);
        void Visit(Parallelogram parallelogram, Graphics g);
        void Visit(Ellipse ellipse, Graphics g);
        void Visit(Circle circle, Graphics g);

    }
}
